﻿Feature: UserAccountPage
	Ability to change the user account details

@mytag
Scenario: Update User Email
	Given I have navigated to the logon page
	And entered the username and password correctly
	And I have logged in correctly
	And I have navigated to the user accont detail page
	When I update the user email to <email>
	And I click update
	Then the email <status> be updated

Examples:

| email                      | status     |
| matthewjones8153@gmail.com | should     |
| matthewjonesgmail.com      | shouldnt   |
| matthewjones@gmail!com     | shouldnt   |