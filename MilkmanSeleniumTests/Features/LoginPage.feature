﻿@loginfeature
Feature: Login Feature
In order to test login functionality as a tester

@loginscenario1
Scenario: User should be able to login with a valid account
	Given I have navigated to the logon page
	And I have entered the username <username> and password <password>
	When I click login
	Then I <status> be navigated to the account page
	
Examples:

| username    | password        | status     |
| 07580097175 | fd9!PPf2ky2rreS | should     |
| 07580097175 | fd9!PPf2ky2rrel | shouldnt   |
| 07580097174 | fd9!PPf2ky2rrel | shouldnt   |

@loginscenario2
Scenario: User should be able to navigate to signup page
	Given I have navigated to the logon page
	When  I have click the signup button
	Then  I should be navigated to the sign up page

@loginscenario3
Scenario: User should be able to navigate to the forgot password page
	Given I have navigated to the logon page
	When  I click on the forgot password button
	Then  I should be navigated to the forgot password page
	