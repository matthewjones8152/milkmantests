﻿using System;
using TechTalk.SpecFlow;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using NUnit.Framework;
using MilkmanSeleniumTests.Helpers;

namespace MilkmanSeleniumTests.Steps
{
    [Binding]
    public class LoginFeatureSteps
    {
        private readonly IWebDriver driver;
        private readonly HelperClass _helpers;

        public LoginFeatureSteps(IWebDriver driver, HelperClass helpers)
        {
            this.driver = driver;
            _helpers = helpers;
        }

        [Given(@"I have navigated to the logon page")]
        public void GivenIHaveNavigatedToTheLogonPage()
        {
            driver.Navigate().GoToUrl("https://tst.themodernmilkman.co.uk/users/login");
            Assert.That(driver.Title.ToLower().Equals("the modern milkman"));
        }

        [Given(@"I have entered the username (.*) and password (.*)")]
        public void GivenIHaveEnteredTheCorrectUsernameAndPassword(string username, string password)
        {
            driver.FindElement(By.Id("phoneNo")).SendKeys(username);
            driver.FindElement(By.Id("password")).SendKeys(password);
        }

        [When(@"I click login")]
        public void WhenIClickLogin()
        {
            driver.FindElement(By.Id("btn_submit")).Click();
        }

        [Then(@"I (.*) be navigated to the account page")]
        public void ThenIShouldBeNavigatedToTheAccountPage(string validInvalid)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            if (validInvalid == "should")
            {
                _helpers.WaitUntilElementExists(By.ClassName("single-categories-name"), driver);

                Assert.Multiple(() =>
                {

                    Assert.That(driver.FindElements(By.ClassName("swal2-header")).Count.Equals(0));
                    Assert.That(driver.Url.Equals("https://tst.themodernmilkman.co.uk/"));
                });

            }
            else
            {
                _helpers.WaitUntilElementExists(By.ClassName("swal2-header"),driver);

                Assert.Multiple(() =>
                {
                    Assert.That(driver.FindElement(By.ClassName("swal2-header")).Displayed.Equals(true));
                });
            }
        }

        [When(@"I have click the signup button")]
        public void WhenIHaveClickTheSignupButton()
        {
            driver.FindElement(By.LinkText("Sign Up")).Click();
        }

        [Then(@"I should be navigated to the sign up page")]
        public void ThenIShouldBeNavigatedToTheSignUpPage()
        {
            Assert.Multiple(() =>
            {
                Assert.That(driver.Url.Equals("https://tst.themodernmilkman.co.uk/users/check-postcode"), $"User was navigated to: {driver.Url}");
                Assert.That(driver.FindElements(By.Id("checkPostcode")).Count.Equals(1),
                    $"Driver found {driver.FindElements(By.Id("checkPostcode")).Count} elements with the id 'checkPostcode'");
            });
        }

        [When(@"I click on the forgot password button")]
        public void WhenIClickOnTheForgotPasswordButton()
        {
            driver.FindElement(By.LinkText("Forgot Password")).Click();
        }



        [Then(@"I should be navigated to the forgot password page")]
        public void ThenIShouldBeNavigatedToTheForgotPasswordPage()
        {
            Assert.Multiple(() =>
            {
                Assert.That(driver.FindElement(By.Id("forgtpassword")).Displayed);
                Assert.That(driver.Url.ToLower().Equals("https://tst.themodernmilkman.co.uk/users/forgot-password"));
            });
        }
    }
}

