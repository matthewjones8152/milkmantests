﻿using MilkmanSeleniumTests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace MilkmanSeleniumTests.Steps
{
    [Binding]
    public class UserAccountPageSteps
    {
        private readonly IWebDriver driver;
        private readonly HelperClass _helpers;
        public UserAccountPageSteps(IWebDriver driver, HelperClass helpers)
        {
            this.driver = driver;
            _helpers = helpers;
        }

        [Given(@"entered the username and password correctly")]
        public void GivenEnteredTheUsernameAndPasswordCorrectly()
        {
            driver.FindElement(By.Id("phoneNo")).SendKeys("07580097175");
            driver.FindElement(By.Id("password")).SendKeys("fd9!PPf2ky2rreS");
        }
        
        [Given(@"I have logged in correctly")]
        public void GivenIHaveLoggedInCorrectly()
        {
            driver.FindElement(By.Id("btn_submit")).Click();
            _helpers.WaitUntilElementExists(By.ClassName("single-categories-name"), driver);
        }
        
        [Given(@"I have navigated to the user accont detail page")]
        public void GivenIHaveNavigatedToTheUserAccontDetailPage()
        {
            driver.Navigate().GoToUrl("https://tst.themodernmilkman.co.uk/users/update-profile");
            _helpers.WaitUntilElementExists(By.Id("email"), driver);
        }
        
        [When(@"I update the user email to (.*)")]
        public void WhenIUpdateTheUserEmailTo(string email)
        {
            driver.FindElement(By.Id("email")).Clear();
            driver.FindElement(By.Id("email")).SendKeys(email);
            }
        
        [When(@"I click update")]
        public void WhenIClickUpdate()
        {
            driver.FindElement(By.Id("update_profile")).Click();
        }
        
        [Then(@"the email (.*) be updated")]
        public void ThenTheEmailBeUpdated(string status)
        {
            if (status == "should")
            {
                _helpers.WaitUntilElementExists(By.ClassName("swal2-success-ring"), driver);
                Assert.That(driver.FindElement(By.ClassName("swal2-success-ring")).Displayed);
            }
            else
            {
                Assert.That(driver.FindElement(By.Id("error-otp")).Displayed);
            }
        }
    }
}
